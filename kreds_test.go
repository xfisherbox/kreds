package main

import "testing"


var kreds = NewClient("http://a:b@localhost:14022/", 6);


// 0.00001 kreds min

func TestSetFee(t *testing.T) {

  resp, err := kreds.SetFee(0.00001)
  if err != nil {
     t.Errorf("setFee error: %+v", err)
     t.FailNow()
  }
  t.Logf("setFee result: %v", resp)
}

func TestCreateAddress(t *testing.T) {

	resp, err := kreds.CreateAddress()
	if err != nil {
		 t.Errorf("createAddress error: %+v", err)
		 t.FailNow()
	}
	t.Logf("createAddress result: %v", resp)
}

func TestGetBalance(t *testing.T) {

  resp, err := kreds.GetBalance()
  if err != nil {
     t.Errorf("getBalance error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalance result: %v", resp)
}

func TestGetWalletInfo(t *testing.T) {
  resp, err := kreds.GetWalletInfo()
  if err != nil {
     t.Errorf("getWalletInfo error: %+v", err)
     t.FailNow()
  }
  t.Logf("getWalletInfo result: %+v", resp)
}

func TestGetBalanceByAddress(t *testing.T) {
  resp, err := kreds.GetBalanceByAddress("KDTsrEwtUCYbqoEgrkR4vAssw3ncjbH1fV")
  if err != nil {
     t.Errorf("getBalanceByAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("getBalanceByAddress result: %v", resp)
}


func TestSendToAddress(t *testing.T) {

  addr := "KH8t7e3Scgc5UYCWsEChL8XyApSbuLFnPU"
  resp, err := kreds.SendToAddress(addr, 0.001)
  if err != nil {
     t.Errorf("sendToAddress error: %+v", err)
     t.FailNow()
  }
  t.Logf("sendToAddress result: %v", resp)
}



func TestGetTransaction(t *testing.T) {
  resp, err := kreds.GetTransaction("0830a355f89acc91cb113004ec3e64027f636731077027c1946b7e79aa4f1fe7")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}

func TestCheckTransaction(t *testing.T) {
  resp, err := kreds.CheckTransaction("0830a355f89acc91cb113004ec3e64027f636731077027c1946b7e79aa4f1fe7")
  if err != nil {
     t.Errorf("getTransaction error: %+v", err)
     t.FailNow()
  }
  t.Logf("getTransaction result: %v", resp)
}
